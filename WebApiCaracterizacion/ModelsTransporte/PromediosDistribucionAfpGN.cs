﻿namespace WebApiCaracterizacion.ModelsTransporte
{
    public class PromediosDistribucionAfpGN
    {
        public string municipio { get; set; }
        public string dato { get; set; }
        public int cantidad { get; set; }
        public double porcentaje { get; set; }
    }
}
