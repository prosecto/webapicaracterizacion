﻿namespace WebApiCaracterizacion.ModelsGanaderia
{
    public class PromediosProduccionArrozGN
    {
        public string tipo_plantilla { get; set; }
        public string municipio { get; set; }
        public string dato { get; set; }
        public int cantidad { get; set; }
        public double porcentaje { get; set; }
    }
}
